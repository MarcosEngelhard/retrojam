﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSand : MonoBehaviour
{
    private enum QuickSandState
    {
        Static, Animated,
    }
    [SerializeField]private QuickSandState sandState;
    private Animator animator;
    [SerializeField] private float timeBetweenAnimations = 7f;
    private float initialTime;
    private bool isTurnOn = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        switch (sandState)
        { // If it static don't play animation, if not play the animation
            case QuickSandState.Static:
                animator.enabled = false;
                break;
            case QuickSandState.Animated:
                animator.enabled = true;
                break;
        }
        initialTime = timeBetweenAnimations; // save time betweem animations
    }

    // Update is called once per frame
    void Update()
    {
        switch (sandState)
        {
            case QuickSandState.Animated:
                ChangeAnimation();
                break;
        }
        
    }
    private void ChangeAnimation()
    {
        timeBetweenAnimations -= Time.deltaTime;
        if (timeBetweenAnimations < 0)
        {
            timeBetweenAnimations = 0;
            timeBetweenAnimations += initialTime;
            isTurnOn = !isTurnOn;
            if (isTurnOn)
            {
                animator.SetBool("TurnOn", true);
            }
            else
            {
                animator.SetBool("TurnOn", false);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            if (!player.OnLiana)
            {
                player.ToSpawn(); // player loses one hp
            }
           
        }
    }
}

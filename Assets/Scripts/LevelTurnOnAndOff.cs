﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTurnOnAndOff : MonoBehaviour
{
    private List<GameObject> childs = new List<GameObject>();
    [SerializeField] private Transform CameraPos;
    private BoxCollider2D bc;
    private Transform player;
    public bool hasPlayer = false;
    private void Awake()
    {
        bc = GetComponent<BoxCollider2D>();
        player = FindObjectOfType<Player>().transform;
        foreach (Transform child in transform)
        {
            childs.Add(child.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        TurnOff();
    }

    // Update is called once per frame
    void Update()
    {
        ManageBoundary();
    }
    void ManageBoundary()
    {
        if(bc.bounds.min.x < player.position.x && bc.bounds.max.x > player.position.x) // If Player is inside of the rigidbody
        {
            CameraPos.gameObject.SetActive(true);
        }
        else
        {
            CameraPos.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            hasPlayer = true;
            foreach(GameObject child in childs)
            {
                child.SetActive(true);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            if (!player.OnLiana)
            {
                TurnOff();
            }

        }
    }
    public void TurnOff()
    {
        // Turn off what the level has as child
        hasPlayer = false;
        foreach (GameObject child in childs)
        {
            if (child != null)
            {
                child.SetActive(false);
            }

        }
    }
}

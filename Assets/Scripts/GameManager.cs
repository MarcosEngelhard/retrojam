﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private int currentPoints = 2000;
    [SerializeField] private Text pointsText;
    [SerializeField]private int hours = 20;
    [SerializeField]private float currentTime = 0;
    [SerializeField]private int muchminuteshaveaTime = 59;
    [SerializeField] private Text hourText;
    private bool isGameOver = false;
    [SerializeField]private int treasuresFound = 0;
    public float effectDurability = 5f; // see how long the effect is on
    private AudioSource audioSource;
    [SerializeField] AudioClip rewardClipSound;
    [SerializeField] private AudioClip winClip;
    private Player player;
    private Animator playerAnimator;
    [SerializeField] private bool hasWon = false;

    public bool IsGameOver
    {
        get
        {
            return isGameOver;
        }
        set
        {
            isGameOver = value;
        }
    }
    private bool isCounting = false;

    private void Awake()
    {
        if (instance == null)
            instance = this; // a singleton
        pointsText.text = currentPoints.ToString();
        hourText.text = hours.ToString() + "-" + currentTime.ToString();
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        playerAnimator = player.GetComponent<Animator>();
        StartCoroutine(StartCountdown());
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameOver)
            return;
        TimeDecreasing();
    }
    private void TimeDecreasing()
    {
        if (isCounting)
        {
            currentTime -= Time.deltaTime;
            if (currentTime < 0 && hours > 0)
            {
                currentTime = 0;
                currentTime += muchminuteshaveaTime;
                hours--;
            }
            CheckIfFinished();
            hourText.text = hours.ToString() + "-" + currentTime.ToString("00");
        }      
    }
    private void CheckIfFinished()
    {
        if(hours <= 0 && currentTime <= 0)
        {
            isGameOver = true;
            Time.timeScale = 0f;
        }
    }
    public void EarnPoints(int amountPointsGiven)
    {
        currentPoints += amountPointsGiven;
        pointsText.text = currentPoints.ToString();
    }
    public void LosePoints(int amountPointsTaken)
    {
        currentPoints -= amountPointsTaken;
        if(currentPoints <= 0)
        {
            currentPoints = 0;
        }
        pointsText.text = currentPoints.ToString();
    }
    private IEnumerator StartCountdown()
    {
        yield return new WaitForSeconds(1f);
        isCounting = true;
    }
    public void AddTreasureFound()
    {
        audioSource.PlayOneShot(rewardClipSound);
        treasuresFound++;
        if(treasuresFound >= 32)
        {
            hasWon = true;
            audioSource.PlayOneShot(winClip);
            Time.timeScale = 0f;
        }
    }
    public void EffectPoison()
    {
        StopCoroutine(EffectOverTime());
        StartCoroutine(EffectOverTime());
        player.SetAnAnimator("Poison");
    }
    public void EffectFire()
    {
        StopCoroutine(EffectOverTime());        
        StartCoroutine(EffectOverTime());
        player.SetAnAnimator("Fire");
    }
    public void LimpEffect()
    {
        StopCoroutine(EffectOverTime());
        StartCoroutine(EffectOverTime());
        player.SetAnAnimator("Mancar");
    }
    public void DisableEffectPoison()
    {
        StopAllCoroutines();
    }
    public void DisableEffectFire()
    {
        StopAllCoroutines();
    }
    public void DisableLimpEffect()
    {
        StopAllCoroutines();
    }
    private IEnumerator EffectOverTime()
    {
        float howtimetoMake = effectDurability;
        while(howtimetoMake >= 0)
        {
            LosePoints(2);
            yield return null;
            howtimetoMake -= Time.deltaTime;
        }
    }
}

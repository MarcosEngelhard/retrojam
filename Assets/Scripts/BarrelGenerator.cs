﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelGenerator : MonoBehaviour
{
    private List<Barrel> Barrels = new List<Barrel>();
    //private bool isVisible = false;
    [SerializeField] private float timeBetweenBarrels = 0.5f;
    private bool canDeploy = true;
    private Player player;
    private LevelTurnOnAndOff level;

    private void Awake()
    {
        level = transform.parent.GetComponent<LevelTurnOnAndOff>();
        player = FindObjectOfType<Player>();
        foreach(Transform child in transform)
        {
            if(child.TryGetComponent<Barrel>(out Barrel barrel))
            {
                Barrels.Add(barrel);
                child.gameObject.SetActive(false);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (/*isVisible &&*/ canDeploy && level.hasPlayer)
        {
            foreach(Barrel barrel in Barrels)
            {
                if (barrel.gameObject.activeInHierarchy)
                {
                    return;
                }
            }
            StartCoroutine(DeployBarrels());

        }
        
    }
    private IEnumerator DeployBarrels()
    {
        canDeploy = false;
        foreach(Barrel barrel in Barrels)
        {
            barrel.gameObject.SetActive(true);
            barrel.transform.parent = transform.parent;
            yield return new WaitForSeconds(timeBetweenBarrels);
        }
        canDeploy = true;
    }
    
    //private void OnBecameInvisible()
    //{
    //    isVisible = false;
    //}
    //private void OnBecameVisible()
    //{
    //    isVisible = true;
    //}

}

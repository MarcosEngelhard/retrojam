﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    private BoxCollider2D cameraBC;
    private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        FollowPlayer();
    }
    private void FollowPlayer()
    {
        GameObject cameraPosition = GameObject.Find("CameraPosition");
        if (cameraPosition != null)
        {
            Camera.main.transform.position = cameraPosition.transform.position;
        }
    }
}

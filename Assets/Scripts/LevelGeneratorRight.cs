﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneratorRight : MonoBehaviour
{
    [SerializeField] private float Player_Distance_Spawn_Level = 10f; 
    [SerializeField] private Transform levelPart_Start; // Thebeginning of the level
    Transform lastLevelPartTransform;
    [SerializeField] private Player player;
    [SerializeField] private int startSpawnLevelParts = 5;
    private Vector3 lastEndPosition;

    private void Awake()
    {
        lastEndPosition = levelPart_Start.Find("EndPosRight").position;
        for(int i = 0; i < startSpawnLevelParts; i++)
        {
            SpawnLevelPart();
        }
    }
    private void SpawnLevelPart()
    {
        Transform chosenLevelPart = LevelList.instance.Get();
        lastLevelPartTransform = SpawnLevelPart(chosenLevelPart, lastEndPosition);
        lastEndPosition = lastLevelPartTransform.Find("EndPosRight").position;
    }
    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition)
    {
        //Spawn A level
        Transform levelPartTransform =  Instantiate(levelPart, spawnPosition, Quaternion.identity);
        return levelPartTransform;
    }
    // Start is called before the first frame update
    void Start()
    {   
    }
    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs(player.transform.position.x - lastEndPosition.x) < Player_Distance_Spawn_Level)
        {
            // Spawn level part when player closes to the end
            SpawnLevelPart();
        }
    }
}

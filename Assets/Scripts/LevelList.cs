﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelList : MonoBehaviour
{
    public static LevelList instance;
    [SerializeField] private List<Transform> levelPartList;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public Transform Get()
    {
        return levelPartList[Random.Range(0, levelPartList.Count)];
    }
}

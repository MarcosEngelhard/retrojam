﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private float inputX;
    private Rigidbody2D rb;
    [SerializeField] private float speed = 4f;
    [SerializeField] private float jumpheight = 15f;
    private BoxCollider2D bc;
    [SerializeField]private float extraHeightSize = 0.1f;
    [SerializeField] private LayerMask groundMask;
    private bool isJumping = false;
    public int health = 3;
    public Transform spawnPoint;
    [SerializeField]private LayerMask WhatIsLadder;
    [SerializeField] private float distance;
    private bool isClimbing = false;
    private float inputVertical;
    private float defaultGravity;
    private bool isOnLiana = false;
    public bool OnLiana
    {
        get { return isOnLiana; }
    }
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private bool isFacingRight = true;
    private AudioSource audioSource;
    [SerializeField] private AudioClip jumpSound;
    [SerializeField] private AudioClip upLadder;
    [SerializeField] private AudioClip downLadder;
    [SerializeField] private AudioClip lianaSwing;
    [SerializeField] private AudioClip deathClip;
    [SerializeField] private AudioClip poisonClip;
    [SerializeField] private AudioClip fireClip;
    private LevelLoaded loaded;
    [SerializeField] private float cannotOnLianaDuration = 0.5f;
    private bool canTakeaLiana = true;
    [SerializeField]private Image[] healthUI;
    private GameObject liana;

    // Start is called before the first frame update
    void Start()
    {
        // Get box collider rigidbody 2D and animator component
        bc = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        defaultGravity = rb.gravityScale;
        loaded = FindObjectOfType<LevelLoaded>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.IsGameOver)
        {
            inputX = Input.GetAxisRaw("Horizontal");
            isJumping = Input.GetButtonDown("Jump");
            Jump();
            ClimbLadder();
            IsOnLiana();
            LookForASpawn();
        }        
    }
    private void FixedUpdate()
    {
        if (!GameManager.instance.IsGameOver)
        {
            MovePlayer();
        }        
    }
    private void Jump()
    {
        if (IsGrounded() && isJumping)
        {
            // Jump
            rb.AddForce(new Vector2(rb.velocity.x, jumpheight * rb.mass), ForceMode2D.Impulse);
            audioSource.PlayOneShot(jumpSound);
            animator.SetBool("Jump", true);

        }
        else if(rb.velocity.y == 0) // when is not either jumping or falling
        {
            animator.SetBool("Jump", false);
        }   
    }
    private void MovePlayer()
    {
        if(rb.bodyType == RigidbodyType2D.Dynamic)
        {
            rb.velocity = new Vector2(inputX * speed, rb.velocity.y);

            if (rb.velocity.x != 0)
            {
                animator.SetBool("Walk", true);
                if (rb.velocity.x > 0 && !isFacingRight)
                {
                    Flip();
                }
                else if (rb.velocity.x < 0 && isFacingRight)
                {
                    Flip();
                }
            }
            else
            {
                animator.SetBool("Walk", false);
            }

        }

            
    }
    private void Flip()
    {
        spriteRenderer.flipX = !spriteRenderer.flipX;
        isFacingRight = !isFacingRight;
    }
    private bool IsGrounded() // true if is grounded, false if not
    {
        RaycastHit2D raycashit = Physics2D.BoxCast(bc.bounds.center, bc.bounds.size, 0f, Vector2.down,  extraHeightSize, groundMask);
        Color raycolor;
        if(raycashit.collider != null)
        {
            raycolor = Color.green;
        }
        else
        {
            raycolor = Color.red;
        }
        Debug.DrawRay(bc.bounds.center + new Vector3(bc.bounds.extents.x, 0), Vector3.down * (bc.bounds.extents.y + extraHeightSize), raycolor);
        Debug.DrawRay(bc.bounds.center - new Vector3(bc.bounds.extents.x, 0), Vector3.down * (bc.bounds.extents.y + extraHeightSize), raycolor);
        Debug.DrawRay(bc.bounds.center - new Vector3(bc.bounds.extents.x, bc.bounds.extents.y + extraHeightSize), Vector3.right * (bc.bounds.extents.y), raycolor);
        return raycashit.collider != null;
    }
    public void ToSpawn()
    {
        health--;
        if(health < 3)
        {
            healthUI[2].gameObject.SetActive(false);
        }
        if (health < 2)
        {
            healthUI[1].gameObject.SetActive(false);
        }
        if (health < 1)
        {
            healthUI[0].gameObject.SetActive(false);
        }

        audioSource.PlayOneShot(deathClip);
        if(health <= 0)
        {
            GameManager.instance.IsGameOver = true;
            Time.timeScale = 0;
            return;
        }
        else
        {
            loaded.LoadNextLevel();
        }
        transform.SetParent(null);
        transform.position = spawnPoint.position;
        transform.rotation = Quaternion.Euler(Vector3.zero);
        isOnLiana = false;
    }
    private void LookForASpawn()
    { // Search for a single spawn
        if(GameObject.Find("Spawn")!= null)
        {
            Transform spawnFound = GameObject.Find("Spawn").transform;
            if (spawnFound != null)
            {
                spawnPoint = spawnFound.transform;
            }
        }
        
    }
    private void ClimbLadder()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.up, distance, WhatIsLadder);
        RaycastHit2D hitGroundInfo = Physics2D.Raycast(transform.position, Vector2.left, distance, groundMask);
        if(hitInfo.collider != null)
        {
            animator.SetBool("Ladder", true);
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
            { // Now is climbing on the ladder
                isClimbing = true;
                
            }
            if( Input.GetKeyDown(KeyCode.D) && hitGroundInfo.collider != null)
            { // do a "jump"
                rb.AddForce(new Vector2( 2.5f, jumpheight * rb.mass), ForceMode2D.Impulse);
                isClimbing = false;
            }
            if((Input.GetKeyDown(KeyCode.A) && hitGroundInfo.collider != null))
            { // do a "jump"
                rb.AddForce(new Vector2(-2.5f, jumpheight * rb.mass), ForceMode2D.Impulse);
                isClimbing = false;
            }
            if ((Input.GetKeyDown(KeyCode.Space) && hitGroundInfo.collider != null))
            { // do a "jump"
                rb.AddForce(new Vector2(0, jumpheight * rb.mass), ForceMode2D.Impulse);
                isClimbing = false;
            }
        }
        if (isClimbing && hitInfo.collider != null)
        { // can climb vertically only
            inputVertical = Input.GetAxisRaw("Vertical");
            rb.velocity = new Vector2(rb.velocity.x, inputVertical * speed);
            rb.gravityScale = 0; // don't pull downwards
            if(rb.velocity.y > 0)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.PlayOneShot(upLadder);
                    animator.SetBool("LadderIdle", false);
                }
            }
            else if(rb.velocity.y < 0)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.PlayOneShot(downLadder);
                    animator.SetBool("LadderIdle", false);
                }
            }
            else
            {
                audioSource.Stop();
                animator.SetBool("LadderIdle", true);
            }
        }
        else
        { // reset gravityscale
            rb.gravityScale = defaultGravity;
            animator.SetBool("Ladder", false);
            animator.SetBool("LadderIdle", false);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Liana"))
        {
            if (canTakeaLiana)
            {
                isOnLiana = true;
                transform.SetParent(collision.transform, true);
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.Euler(Vector3.zero);
                rb.gravityScale = 0;
                animator.SetBool("Liana", true);
                liana = collision.transform.transform.parent.parent.gameObject;
                rb.bodyType = RigidbodyType2D.Static;
            }
            
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Liana"))
        {
            if (isOnLiana && transform.parent != null)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.PlayOneShot(lianaSwing);
                }
            }           
        }
    }
    private void IsOnLiana()
    {
        if (isOnLiana)
        {
            
            if (Input.anyKeyDown)
            {
                
                if ((Input.GetKeyDown(KeyCode.Space)))
                { // do a "jump"
                    LeaveLiana();
                    rb.bodyType = RigidbodyType2D.Dynamic;
                    rb.AddRelativeForce(transform.up * jumpheight * rb.mass, ForceMode2D.Impulse);
                }
            }
            else
            {
                //transform.localPosition = Vector3.zero;
                //transform.rotation = Quaternion.Euler(Vector3.zero);
            }

        }
    }
    private void LeaveLiana()
    {
        isOnLiana = false;
        transform.SetParent(null);
        rb.gravityScale = defaultGravity;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
        animator.SetBool("Liana", false);
        audioSource.Stop();
        StartCoroutine(CannotTakeLiana());
    }
    IEnumerator CannotTakeLiana()
    {
        canTakeaLiana = false;
        yield return new WaitForSeconds(cannotOnLianaDuration);
        canTakeaLiana = true;
    }
    public void LimpOn()
    {
        animator.SetBool("Mancar", true);
    }
    public void LimpOff()
    {
        animator.SetBool("Mancar", false);
    }
    public void PlayerPoisonClip() // by animator event
    {
        audioSource.PlayOneShot(poisonClip);
    }
    public void PlayerFireClip()
    {
        audioSource.PlayOneShot(fireClip);
    }
    public void SetAnAnimator(string animation)
    {
        StartCoroutine(AnimationHapenning(animation));
    }
    IEnumerator AnimationHapenning(string name)
    {
        animator.SetBool(name, true);
        yield return new WaitForSeconds(GameManager.instance.effectDurability);
        animator.SetBool(name, false);
    }

}

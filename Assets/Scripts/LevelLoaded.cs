﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoaded : MonoBehaviour
{
    [SerializeField] private float transitionTime = 1f;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex));
    }
    IEnumerator LoadLevel(int levelIndex)
    {
        animator.SetTrigger("Start");
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(transitionTime);
        animator.SetTrigger("End");
        Time.timeScale = 1f;
        //SceneManager.LoadScene(levelIndex);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutSideEffect : MonoBehaviour
{
    public enum KindEffect
    {
        Fire, Poison
    }
    [SerializeField] private KindEffect effect;
    private delegate void ApplyEffect(Player target);
    private ApplyEffect apply;
    private delegate void DisableEffect();
    private DisableEffect disableEffect;
    private Player playerBehaviour;
    private Animator animator;
    private AudioSource audioSource;
    [SerializeField]private AudioClip fireClip;
    [SerializeField]private AudioClip poisonClip;
    private void Awake()
    {
        playerBehaviour = FindObjectOfType<Player>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        switch (effect)
        {
            case KindEffect.Fire:
                apply = PlayerOnFire;
                disableEffect = GameManager.instance.DisableEffectFire;
                break;
            case KindEffect.Poison:
                apply = PlayerPoisoned;
                disableEffect = GameManager.instance.DisableEffectPoison;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {        
    }
    private void PlayerOnFire(Player target)
    {
        audioSource.PlayOneShot(fireClip);
        GameManager.instance.EffectFire();
    }
    private void PlayerPoisoned(Player target)
    {
        audioSource.PlayOneShot(poisonClip);
        GameManager.instance.EffectPoison();
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Player>(out Player player))
        {
            animator.SetTrigger("Attack");
            apply?.Invoke(player);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Player>(out Player player))
        {
            disableEffect?.Invoke();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion : MonoBehaviour
{
    private Transform target;
    private Rigidbody2D rb;
    [SerializeField]private float speed = 4f;
    [SerializeField] private BoxCollider2D notTriggerCollider;
    private Animator animator;
    private bool isFacingLeft = true;
    private SpriteRenderer spriteRenderer;
    private AudioSource audioSource;
    [SerializeField] private AudioClip hitClip;
    private enum State
    {
        Chase, Idle, Attack
    }
    [SerializeField]private State state;
    private Vector3 direction;
    private Vector3 initialPosition;
    private BoxCollider2D playerBC;
    private void Awake()
    {
        target = FindObjectOfType<Player>().transform;
        playerBC = target.gameObject.GetComponent<BoxCollider2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        state = State.Chase;       
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        initialPosition = transform.position;
    }
    private void OnEnable()
    {
        Physics2D.IgnoreCollision(notTriggerCollider, playerBC);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        direction = (target.position - transform.position).normalized;
        direction.y = 0;
        switch (state)
        {
            case State.Idle:
                if (Mathf.Abs(target.position.x - transform.position.x) > 0.2f)
                {
                    state = State.Chase;
                }
                break;
            case State.Chase:
                
                rb.MovePosition((Vector2)transform.position + new Vector2(direction.x * speed * Time.deltaTime,0));
                if(target.position.x > transform.position.x && isFacingLeft)
                {
                    Flip(); // Flip to right
                }
                else if(target.position.x < transform.position.x && !isFacingLeft)
                {
                    Flip(); // Flip to left
                }
                if(Mathf.Abs(target.position.x - transform.position.x) < 0.2f)
                {
                    state = State.Idle;
                }
                break;
            case State.Attack:
                
                break;
        }
    }
    private void Flip()
    {
        // Flip to the opposite side in X axis
        spriteRenderer.flipX = !spriteRenderer.flipX;
        isFacingLeft = !isFacingLeft;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        
        if (collision.gameObject.TryGetComponent<Player>(out Player player))
        {
            
            animator.SetTrigger("Attack");
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(hitClip);
            }
            GameManager.instance.LosePoints(1);
            //player.ToSpawn();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<Player>(out Player player))
        {
            audioSource.Stop();
            animator.SetTrigger("Attack");
            GameManager.instance.LosePoints(1);
        }
    }
    private void OnDisable()
    {
        transform.position = initialPosition;
    }
}

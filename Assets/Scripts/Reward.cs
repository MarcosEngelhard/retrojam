﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reward : MonoBehaviour
{
    [SerializeField]private int givePoints = 30;
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Player get the treasure
            GameManager.instance.EarnPoints(givePoints);
            GameManager.instance.AddTreasureFound();
            Destroy(this.gameObject);
        }
    }
}

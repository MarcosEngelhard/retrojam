﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Barrel : MonoBehaviour
{
    [SerializeField] private float speed = 3f;
    [SerializeField] private int pointsToSteal = 15;
    private Rigidbody2D rb;
    private Player player;
    [SerializeField] public CircleCollider2D notTriggeredBC;
    private BarrelGenerator barrelGenerator;
    private Vector3 initialPos;
    private Animator animator;
    private AudioSource audioSource;
    [SerializeField]private AudioClip hitClip;
    public enum State
    {
        Stop, Moving
    }
    public State state;
    private void Awake()
    {
        barrelGenerator = transform.parent.GetComponent<BarrelGenerator>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initialPos = transform.position;
    }
    private void OnEnable()
    {
        player = FindObjectOfType<Player>();
        Physics2D.IgnoreCollision(notTriggeredBC, player.GetComponent<BoxCollider2D>());
        if (barrelGenerator != null)
        {
            transform.parent = barrelGenerator.transform;
            transform.localPosition = new Vector3(0, 0, 0);
        }
        switch (state)
        {
            case State.Stop:
                animator.SetBool("Rolling", false);
                break;
            case State.Moving:
                animator.SetBool("Rolling", true);
                break;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (state)
        {
            case State.Stop:
                break;
            case State.Moving:
                rb.velocity = -Vector3.right * speed;
                break;
        }      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            GameManager.instance.LimpEffect();
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Player>(out Player player))
        {
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(hitClip);
            }
            GameManager.instance.LosePoints(1);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            audioSource.Stop();
            GameManager.instance.DisableLimpEffect();
        }
    }
    private void OnBecameInvisible()
    {
        if (transform.position.x > Screen.width)
            return;
        
        gameObject.SetActive(false);      
    }
    private void OnDisable()
    {
        transform.position = initialPos;
    }
}

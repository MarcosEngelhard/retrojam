﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crocodile : MonoBehaviour
{
    private bool hasMouseOpen = false;
    [SerializeField] private BoxCollider2D mouseOpenTriggerCollider;
    [SerializeField]private float TimeBetweenFrames = 6f;
    private float initialTimeBetweenFrames;
    [SerializeField] private float changeSpeed = 0.125f;
    private Animator animator;
    private AudioSource audioSource;
    [SerializeField] private AudioClip hitClip;
    private void Awake()
    {
        //Get animator and audiosource components
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        initialTimeBetweenFrames = TimeBetweenFrames;
        
    }

    // Update is called once per frame
    void Update()
    {
        TimeBetweenFrames -= 1 * Time.deltaTime;
        ChangeAnimation();
        
    }
    private void OnEnable()
    {
        animator.SetBool("hasMouseOpen", false);
        mouseOpenTriggerCollider.enabled = false;
        hasMouseOpen = false;
    }
    private void ChangeAnimation()
    {      
        if(TimeBetweenFrames <= 0)
        {
            hasMouseOpen = !hasMouseOpen;
            if (hasMouseOpen)
            {
                animator.SetBool("hasMouseOpen", true);
                mouseOpenTriggerCollider.enabled = true;
            }
            else
            {
                animator.SetBool("hasMouseOpen", false);
                mouseOpenTriggerCollider.enabled = false;
            }
            TimeBetweenFrames = initialTimeBetweenFrames;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<Player>(out Player player))
        {
            audioSource.PlayOneShot(hitClip);
            player.ToSpawn();
        }
    }
}
